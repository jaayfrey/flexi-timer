import 'dart:async';

import 'package:flexi_timer/flexi_timer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

MaterialApp wrapWithMaterialApp(Widget child) {
  return MaterialApp(
    home: Scaffold(
      body: child,
    ),
  );
}

EdgeInsets getEdgeInsets(k, v) => k == const Key('prefix-icon')
    ? EdgeInsets.only(right: v)
    : EdgeInsets.only(left: v);

void main() {
  FlexiTimerController _controller = FlexiTimerController();
  Key prefixIconKey = const Key('prefix-icon');
  Key suffixIconKey = const Key('suffix-icon');
  Color defaultIconColor = Colors.black;
  double defaultIconPadding = 10;
  double defaultIconSize = 24;
  Color defaultTextColor = Colors.black;
  double defaultTextFontSize = 24;

  IconData customIconData = Icons.access_alarm;
  Color customIconColor = Colors.blue;
  double customIconSize = 36;
  double customIconPadding = 20;
  Color customTextColor = Colors.blue;
  double customTextFontSize = 36;

  testWidgets('FlexiTimer - isDisplayIcon is false with default icon',
      (WidgetTester tester) async {
    await tester.pumpWidget(
      wrapWithMaterialApp(
        FlexiTimer(
          controller: _controller,
          isDiplayIcon: false,
        ),
      ),
    );

    expect(find.byType(FlexiTimer), findsOneWidget);
    expect(find.byType(Icon), findsNothing);
  });

  testWidgets('FlexiTimer - isDisplayIcon is false with custom icon',
      (WidgetTester tester) async {
    await tester.pumpWidget(
      wrapWithMaterialApp(
        FlexiTimer(
          controller: _controller,
          isDiplayIcon: false,
          icon: Icon(
            customIconData,
          ),
        ),
      ),
    );

    expect(find.byType(FlexiTimer), findsOneWidget);
    expect(find.byType(Icon), findsNothing);
  });

  testWidgets(
      'FlexiTimer - isDisplayIcon is true with default icon and default icon properties',
      (WidgetTester tester) async {
    await tester.pumpWidget(
      wrapWithMaterialApp(
        FlexiTimer(
          controller: _controller,
          isDiplayIcon: true,
        ),
      ),
    );

    expect(find.byType(FlexiTimer), findsOneWidget);
    expect(find.byType(Icon), findsOneWidget);
    expect(
        find.byWidgetPredicate((widget) =>
            widget is Icon &&
            widget.color == defaultIconColor &&
            widget.size == defaultIconSize),
        findsOneWidget);
    expect(
        find.byWidgetPredicate(
            (widget) => widget is Container && widget.key == suffixIconKey),
        findsOneWidget);

    expect(
        find.byWidgetPredicate((widget) =>
            widget is Padding &&
            widget.padding == getEdgeInsets(suffixIconKey, defaultIconPadding)),
        findsOneWidget);
  });

  testWidgets(
      'FlexiTimer - isDisplayIcon is true with default icon and custom icon properties',
      (WidgetTester tester) async {
    await tester.pumpWidget(
      wrapWithMaterialApp(
        FlexiTimer(
          controller: _controller,
          isDiplayIcon: true,
          iconPadding: customIconPadding,
          iconPosition: IconPosition.prefix,
          iconColor: customIconColor,
          iconSize: customIconSize,
        ),
      ),
    );

    expect(find.byType(FlexiTimer), findsOneWidget);
    expect(find.byType(Icon), findsOneWidget);
    expect(
        find.byWidgetPredicate((widget) =>
            widget is Icon &&
            widget.color == customIconColor &&
            widget.size == customIconSize),
        findsOneWidget);
    expect(
        find.byWidgetPredicate(
            (widget) => widget is Container && widget.key == prefixIconKey),
        findsOneWidget);
    expect(
        find.byWidgetPredicate((widget) =>
            widget is Padding &&
            widget.padding == getEdgeInsets(prefixIconKey, customIconPadding)),
        findsOneWidget);
  });

  testWidgets('FlexiTimer - isDisplayIcon is true with custom icon',
      (WidgetTester tester) async {
    await tester.pumpWidget(
      wrapWithMaterialApp(
        FlexiTimer(
          controller: _controller,
          isDiplayIcon: true,
          icon: Icon(
            customIconData,
            color: customIconColor,
            size: customIconSize,
          ),
          iconPosition: IconPosition.prefix,
          iconPadding: customIconPadding,
        ),
      ),
    );

    expect(find.byType(FlexiTimer), findsOneWidget);
    expect(find.byType(Icon), findsOneWidget);
    expect(
        find.byWidgetPredicate((widget) =>
            widget is Icon &&
            widget.icon == customIconData &&
            widget.size == customIconSize &&
            widget.color == customIconColor),
        findsOneWidget);
    expect(
        find.byWidgetPredicate(
            (widget) => widget is Container && widget.key == prefixIconKey),
        findsOneWidget);
    expect(
        find.byWidgetPredicate((widget) =>
            widget is Padding &&
            widget.padding == getEdgeInsets(prefixIconKey, customIconPadding)),
        findsOneWidget);
  });

  testWidgets('FlexiTimer - default textstyle', (WidgetTester tester) async {
    await tester.pumpWidget(
      wrapWithMaterialApp(
        FlexiTimer(
          controller: _controller,
        ),
      ),
    );

    expect(find.byType(FlexiTimer), findsOneWidget);
    expect(
        find.byWidgetPredicate((widget) =>
            widget is Text &&
            widget.style!.color == defaultTextColor &&
            widget.style!.fontSize == defaultTextFontSize),
        findsOneWidget);
  });

  testWidgets('FlexiTimer - custom textstyle', (WidgetTester tester) async {
    await tester.pumpWidget(
      wrapWithMaterialApp(
        FlexiTimer(
          controller: _controller,
          textStyle: TextStyle(
            color: customTextColor,
            fontSize: customTextFontSize,
          ),
        ),
      ),
    );

    expect(find.byType(FlexiTimer), findsOneWidget);
    expect(
        find.byWidgetPredicate((widget) =>
            widget is Text &&
            widget.style!.color == customTextColor &&
            widget.style!.fontSize == customTextFontSize),
        findsOneWidget);
  });

  testWidgets('FlexiTimer - default timer format', (WidgetTester tester) async {
    await tester.pumpWidget(
      wrapWithMaterialApp(
        FlexiTimer(
          controller: _controller,
        ),
      ),
    );

    expect(find.byType(FlexiTimer), findsOneWidget);
    expect(find.text('00'), findsOneWidget);
  });

  testWidgets('FlexiTimer - custom timer format', (WidgetTester tester) async {
    await tester.pumpWidget(
      wrapWithMaterialApp(
        FlexiTimer(
          controller: _controller,
          timerFormat: TimerTextFormat.Hms,
        ),
      ),
    );

    expect(find.byType(FlexiTimer), findsOneWidget);
    expect(find.text('00:00:00'), findsOneWidget);
  });

  testWidgets('FlexiTimer - with controller and no duration',
      (WidgetTester tester) async {
    await tester.pumpWidget(
      wrapWithMaterialApp(
        FlexiTimer(
          controller: _controller,
        ),
      ),
    );

    expect(find.byType(FlexiTimer), findsOneWidget);
    expect(find.text('00'), findsOneWidget);
    expect(_controller.isTimerRunning, false);
  });

  testWidgets('FlexiTimer - with controller and duration',
      (WidgetTester tester) async {
    await tester.pumpWidget(
      wrapWithMaterialApp(
        FlexiTimer(
          controller: _controller,
          duration: const Duration(seconds: 5),
        ),
      ),
    );
    await tester.pump(const Duration(seconds: 1));
    expect(find.byType(FlexiTimer), findsOneWidget);
    expect(find.text('04'), findsOneWidget);
    expect(_controller.isTimerRunning, true);
    await tester.pump(const Duration(seconds: 5));
    expect(find.text('00'), findsOneWidget);
  });

  testWidgets('FlexiTimer - with onComplete', (WidgetTester tester) async {
    final onCompleteCompleter = Completer<bool>();
    await tester.pumpWidget(
      wrapWithMaterialApp(
        FlexiTimer(
          controller: _controller,
          duration: const Duration(seconds: 5),
          onComplete: () {
            onCompleteCompleter.complete(true);
          },
        ),
      ),
    );
    await tester.pump(const Duration(seconds: 6));

    expect(find.byType(FlexiTimer), findsOneWidget);
    expect(onCompleteCompleter.isCompleted, isTrue);
  });
}
