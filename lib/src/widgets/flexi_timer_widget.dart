import 'package:flexi_timer/flexi_timer.dart';
import 'package:flexi_timer/src/enums/icon_position.dart';
import 'package:flutter/material.dart';

class FlexiTimer extends StatefulWidget {
  /// Key for FlexiTimer
  final Key? key;

  /// This callback will be executed after timed out
  final VoidCallback? onComplete;

  /// This param allows users to style the text of timer
  final TextStyle textStyle;

  /// This param allows users to change the format of timer
  final TimerTextFormat timerFormat;

  /// This param allows users to personalise the Icon widget
  final Icon? icon;

  /// This param allows users to change the padding between the icon and the timer text
  final double iconPadding;

  /// This param allows users to change the default icon size
  final double? iconSize;

  /// This param allows users to change the default icon color
  final Color? iconColor;

  /// This param allows users to control the visibility of icon
  final bool isDiplayIcon;

  /// This param allows users to change the position of icon
  final IconPosition iconPosition;

  /// This param allows users to control the behaviour of timer
  final FlexiTimerController controller;

  /// This param allows user to personalise the duration of timer
  final Duration? duration;

  const FlexiTimer({
    this.key,
    this.onComplete,
    this.textStyle = const TextStyle(
      color: Colors.black,
      fontSize: 24,
    ),
    this.timerFormat = TimerTextFormat.s,
    this.icon,
    this.iconPadding = 10,
    this.iconSize = 24,
    this.iconColor = Colors.black,
    this.isDiplayIcon = false,
    this.iconPosition = IconPosition.suffix,
    this.duration,
    required this.controller,
  }) : super(key: key);

  @override
  _FlexiTimer createState() => _FlexiTimer();
}

class _FlexiTimer extends State<FlexiTimer> {
  @override
  void initState() {
    super.initState();

    if (widget.duration != null && !widget.controller.isTimerRunning) {
      WidgetsBinding.instance!.addPostFrameCallback(
          (_) => widget.controller.startCountdown(widget.duration!));
    }

    widget.controller
      ..addListener(_setStateListener)
      ..addListener(_onTimeOutListener);
  }

  void _setStateListener() {
    setState(() {});
  }

  void _onTimeOutListener() {
    if (widget.controller.counter == 0 &&
        widget.controller.isTimesUp &&
        widget.onComplete != null) {
      WidgetsBinding.instance?.addPostFrameCallback((_) {
        widget.onComplete!();
      });
    }
  }

  @override
  void dispose() {
    widget.controller
      ..removeListener(_setStateListener)
      ..removeListener(_onTimeOutListener);
    super.dispose();
  }

  @override
  Widget build(BuildContext buildContext) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        widget.iconPosition == IconPosition.prefix && widget.isDiplayIcon
            ? Container(
                key: const Key('prefix-icon'),
                child: getIcon(),
              )
            : Container(),
        Text(
          getDuration,
          style: widget.textStyle,
        ),
        widget.iconPosition == IconPosition.suffix && widget.isDiplayIcon
            ? Container(
                key: const Key('suffix-icon'),
                child: getIcon(),
              )
            : Container(),
      ],
    );
  }

  Widget getIcon() {
    return Padding(
      padding: widget.iconPosition == IconPosition.prefix
          ? EdgeInsets.only(right: widget.iconPadding)
          : EdgeInsets.only(left: widget.iconPadding),
      child: iconWidget(),
    );
  }

  Icon? iconWidget() {
    if (widget.icon == null) {
      return Icon(
        Icons.timer,
        size: widget.iconSize,
        color: widget.iconColor,
      );
    }
    return widget.icon;
  }

  String get getDuration {
    int remaining = widget.controller.counter;
    final Duration remainingDuration = Duration(seconds: remaining);
    final Enum timerFormat = widget.timerFormat;
    late final String duration;

    if (timerFormat == TimerTextFormat.Hms) {
      final hours = remainingDuration.inHours % 60;
      final minutes = remainingDuration.inMinutes % 60;
      final seconds = remaining % 60;

      final hoursString = '$hours'.padLeft(2, '0');
      final minutesString = '$minutes'.padLeft(2, '0');
      final secondsString = '$seconds'.padLeft(2, '0');

      duration = '$hoursString:$minutesString:$secondsString';
    } else if (timerFormat == TimerTextFormat.ms) {
      final minutes = remainingDuration.inMinutes % 60;
      final seconds = remaining % 60;

      final minutesString = '$minutes'.padLeft(2, '0');
      final secondsString = '$seconds'.padLeft(2, '0');

      duration = '$minutesString:$secondsString';
    } else {
      final secondsString = '$remaining'.padLeft(2, '0');
      duration = secondsString;
    }

    return duration;
  }
}
