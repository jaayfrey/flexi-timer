import 'dart:async';

import 'package:flutter/material.dart';

/// Control the behaviour of timer
class FlexiTimerController with ChangeNotifier {
  Timer? _timer;
  int _counter = 0;

  /// Returns the remaining duration of the timer in seconds
  int get counter => _counter;

  bool _isTimesUp = false;

  /// Returns the time out status of timer
  bool get isTimesUp => _isTimesUp;

  /// Sets the status of timer
  set isTimesUp(bool v) {
    _isTimesUp = v;
    notifyListeners();
  }

  bool _isRunning = false;

  /// Returns the on going status of timer
  bool get isTimerRunning => _isRunning;

  /// Starts the countdown of timer
  void startCountdown(Duration duration) async {
    _counter = duration.inSeconds;
    // Cancel old active timer
    if (_timer != null && _timer!.isActive) {
      _timer!.cancel();
    }
    _isRunning = true;
    _isTimesUp = false;

    _timer = startTimer();
    notifyListeners();
  }

  /// Stops the timer
  void stop() async {
    cancelTimer();
    _isTimesUp = false;
    notifyListeners();
  }

  Timer startTimer() =>
      Timer.periodic(const Duration(seconds: 1), (Timer timer) {
        if (_counter > 0) {
          _counter--;
        } else {
          cancelTimer();
          _isTimesUp = true;
        }
        notifyListeners();
      });

  /// Cancel the timer
  void cancelTimer() async {
    _counter = 0;
    _timer?.cancel();
    _timer = null;
    _isRunning = false;
  }

  @override
  void dispose() {
    _timer?.cancel();
    super.dispose();
  }
}
