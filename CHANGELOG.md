## 0.0.3 - Jan. 7, 2022

- Export IconPosition class
- Standardised timer format enums according DateTime format enums

## 0.0.2 - Jan. 6, 2022

- Fixed demo gif
- Added API documentation

## 0.0.1 - Jan. 5, 2022

Initial release of flexi_timer package. Features include:
- Customisable timer
- Start and stop timer with FlexiTimerController
